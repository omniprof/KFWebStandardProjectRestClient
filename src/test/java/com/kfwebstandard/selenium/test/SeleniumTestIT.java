package com.kfwebstandard.selenium.test;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is a sample of a test class for Selenium. As we are using NetBeans the
 * class name needs to end in IT so that the Run Selenium command can recognize
 * a selenium test class
 *
 * @author Ken
 */
public class SeleniumTestIT {

    /**
     * The most basic Selenium test method that tests to see if the page name
     * matches a specific name.
     *
     * @throws Exception
     */
    @Test
    public void testSimple() throws Exception {
        // Normally an executable that matches the browser you are using must
        // be in the classpath. The webdrivermanager library by Boni Garcia
        // downloads the required driver and makes it available
        ChromeDriverManager.getInstance().setup();

        // Create a new instance of the Chrome driver
        // Notice that the remainder of the code relies on the interface,
        // not the implementation.
        WebDriver driver = new ChromeDriver();
        //WebDriver driver = new FirefoxDriver();

        // And now use this to visit this project
        driver.get("http://localhost:8080/KFWebStandardProject/");
        // Alternatively the same thing can be done like this
        // driver.navigate().to("http://localhost:8080/KFWebStandardProject/");

        // Check the title of the page
        // Wait for the page to load, timeout after 10 seconds
        // Anonymous inner class
        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                return d.getTitle().contains("NetBeans");
            }
        });

        //Lambda version
        //(new WebDriverWait(driver, 10)).until((WebDriver d) -> d.getTitle().contains("JPA Displaying Database Tables JPA"));
        //Close the browser
        driver.quit();
    }

}
