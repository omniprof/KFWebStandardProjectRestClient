package com.kfwebstandard.jpacontroller;

import com.kfwebstandard.entities.Fish;
import com.kfwebstandard.service.FishServiceClient;
import java.io.Serializable;
import java.util.List;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 * A magic bean that returns a list of fish
 *
 * @author Ken
 *
 */
@Named
@RequestScoped
public class FishActionBeanJPA implements Serializable {

    public List<Fish> getAll() {
        FishServiceClient client = new FishServiceClient();
        // use if com.owlike.genson is being used
        //List<Fish> fishies = client.getJson(List.class);
        // use if GenericType is wrapping the List of Fish
        List<Fish> fishies = client.getJson2();
        client.close();

        return fishies;
    }
}
