package com.kfwebstandard.service;

import com.kfwebstandard.entities.Fish;
import java.util.List;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;

/**
 * Jersey REST client generated for REST resource:FishiesResource [fishies]<br>
 * USAGE:
 * <pre>
 *        FishServiceClient client = new FishServiceClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author omni_
 */
public class FishServiceClient {

    private final WebTarget webTarget;
    private final Client client;
    private static final String BASE_URI = "http://localhost:8080/KFWebStandardProjectRestServer/webresources";

    public FishServiceClient() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("fishies");
    }

    /**
     * This is the generated client method. Unfortunately it does not work with
     * Collections. There are two solutions. The first can be seen below in
     * getJson2. This method encapsulates the List in a GenericType. The second
     * solution is to leave the code untouched and to add the com.owlike.genson
     * library to the pom.xml.
     *
     * @param <T>
     * @param responseType
     * @return List of Fish
     * @throws ClientErrorException
     */
    public <T> T getJson(Class<T> responseType) throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(responseType);
    }

    /**
     * This is the modified method that uses GenericType.
     *
     * @return List of Fish
     * @throws ClientErrorException
     */
    public List<Fish> getJson2() throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(new GenericType<List<Fish>>() {
        });
    }

    public void close() {
        client.close();
    }

}
